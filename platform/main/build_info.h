#ifndef BUILD_INFO_H
#define BUILD_INFO_H

#define SCFW_BRANCH imx_4.14.78_1.0.0_ga
#define SCFW_BUILD 3018UL
#define SCFW_COMMIT 0x59a6bac0UL
#define SCFW_DATE "May 23 2019"
#define SCFW_DATE2 May_23_2019
#define SCFW_TIME "01:37:15"

#endif
